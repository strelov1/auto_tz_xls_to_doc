<?php
require_once 'excel_reader2.php';
$data = new Spreadsheet_Excel_Reader("tz.xls");

$num_row=2;
$wordarray = [];
$i=0;
	for($row=$num_row; $row <= $data->rowcount($sheet = 0); $row++) {

		$title= $data->val($row, "A");
		$size= $data->val($row, "B");
		$words= $data->val($row, "C");

		if($title == ''){

			$wordarray[$i]['word'][] = $words;
		}else{
			$i++;
			$wordarray[$i]['title'] = $title;
			$wordarray[$i]['size'] = $size;
			$wordarray[$i]['word'][] = $words;
		}

	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf8"/>
	<title></title>
	<style type="text/css">
		@page { margin-left: 3cm; margin-right: 1.5cm; margin-top: 2cm; margin-bottom: 2cm }
		p { margin-bottom: 0.25cm; direction: ltr; color: #00000a; line-height: 120%; text-align: left; orphans: 2; widows: 2 }
		p.western { font-family: "Calibri", serif; font-size: 11pt; so-language: ru-RU }
		p.cjk { font-family: "Calibri"; font-size: 11pt; so-language: en-US }
		p.ctl { font-family: ; font-size: 11pt; so-language: ar-SA }
	</style>
</head>
<body lang="ru-RU" text="#00000a" dir="ltr">
<p class="western" align="center" style="margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>ТЗ
на копирайтинг текста для сайта</b></font></font></p>
<p class="western" style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Правила
написания текстов:</b></font></font></p>
<ol>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Уникальность
	текстов 100%</font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Пишем
	короткие и информативные тексты</font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Неприемлемо
	использовать конструкции из нескольких прилагательных типа: &laquo;Этот
	фееричный блистательный невероятно красивый предмет&raquo;</font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Пишем
	логично и понятно</font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Нужно
	форматировать текст списками, таблицами, заголовками,
	дополнительными абзацами, не обозначенными в ТЗ, если это, по мнению
	копирайтера, может увеличить восприятие текста посетителем.</b></font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Тест
	разбивать на абзацы по 300-450 символов.</b></font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Каждые
	2-3 абзаца делать заголовок (</b></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>H</b></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>2-</b></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>H</b></span></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>3
	должна быть соблюдена иерархия загловков).</b></font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Не
	имеет значение регистр: писать можно ключевые слова, как с
	маленькой, так и с большой, здесь важна грамотность, а не точность.</font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Ключевые
	слова можно использовать как словоформу, перестановку. Главное не
	противоречить правилам русского языка.</font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"> <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Все
	ключевые фразы (слова) должны быть согласованны с точки зрения
	русского языка: окончание, предлоги, заглавные буквы у имен
	собственных.</font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"> <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Корявые
	запросы вид [офис мебель москва],[офис мебель цена] заменяем в
	тексте соответственно  на [мебель в офис в Москве/ мебель для офиса
	в Москве], [цены на офисную мебель / цена на офисную мебель].</font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"> <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Ключевые
	слова в тексте не повторять больше чем указано.</b></font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"> <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Слова,
	которые </font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>не
	желательно</b></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">
	использовать или использовать только </font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>1
	раз:</b></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">
	</font></font>
	</p>
</ol>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">подобный</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">различные</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">на
сегодняшний день</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">можно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">давно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">так
как</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">является</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">позволяет</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">данный</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">безусловно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">всем
известно, что</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">без
сомнения</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">бесспорно
</font></font>
</p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">как
говорится</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">многие/все
(знают, любят, выбирают итд)</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">каждый/
не каждый/ всякий (знает, любит, выбирает итд)</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">к
счастью</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">между
прочим</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">кстати</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">прямо
скажем</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">иными
словами</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">несомненно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">сложно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">несложно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">конечно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">соответственно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">кажется</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">как
водится</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">как
выясняется</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">как
говорится</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">как
оказывается</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">как
полагается</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">оказывается</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">получается</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">помнится</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">разумеется</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">спрашивается</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">что
называется</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">необходимо
напомнить/упомянуть</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">неудивительно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">теперь</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">продажа</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">таким
образом</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">следовательно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">довольно
(компактный, дешевый итд)</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">часто</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">именно
поэтому</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">в
принципе</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">следует
(отметить, обратить внимание)</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">дает
возможность</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">наиболее</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">чаще
всего</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">лучший
вариант</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">менее/более
популярны</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">многочисленные
отзывы</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">при
этом</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">несмотря
на</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">вопреки</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">впрочем</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">данное
устройство</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">в
то же время</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">сегодня</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">все-таки</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">в
любых проявлениях</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">достаточно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">всего
лишь</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">дабы</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">естественно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">разумеется</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">трудно
переоценить/недооценить</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">невозможно
представить</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">что
очень удобно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">более
чем</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">большое
разнообразие</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">именно</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">самый
</font></font>
</p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">очень</font></font></p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<br/>
<br/>

</p>
<p style="margin-left: 1.27cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Структура
текста:</b></font></font></p>
<ol>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"> <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Вводная
	часть &ndash; </b></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><i>описание
	услуги (ее особенностей).</i></font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Выгода
	в данной услуги - </b></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><i>(удобство
	заказа, радость от покупки, дополнительные услуги).</i></font></font></p>
	<li/>
<p style="margin-bottom: 0.28cm; line-height: 108%"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Призыв
	к заказу - </b></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><i>(преимущество
	заказа на сайте, закажите и т.д.)</i></font></font></p>
</ol>
<p style="margin-left: 1.91cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><u><b>Примечание:</b></u></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>
заголовки (</b></font></font><font color="#ff0000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>кроме
указанных в ТЗ</b></font></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>)
остаются на волю копирайтера, но они </b></font></font><font color="#ff0000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>не
должны содержать прямых вхождений ключевых слов!</b></font></font></font></p>
<p style="margin-left: 1.91cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><u><b>___________________________________________________________</b></u></font></font></p>
<p style="margin-left: 1.91cm; margin-bottom: 0.28cm; border-top: none; border-bottom: 1.50pt solid #00000a; border-left: none; border-right: none; padding-top: 0cm; padding-bottom: 0.04cm; padding-left: 0cm; padding-right: 0cm; line-height: 108%">
<font color="#c00000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Каждый
ключи использовать 1 раз в тексте в любой форме на свое усмотрение и
чтобы предложения с колючем были согласованны.  Можно писать как
прямое вхождение, вхождение словоформы, с перестановкой слов, а также
разбавленные вхождения (разбивать одним словом), но нельзя разбивать
вхождение запроса точкой или запятой. Ключи можно использовать в
списках, а также в заголовках </b></font></font></font><font color="#c00000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="ru-RU"><b>второго
уровня . Слова в скобках это - комментарии по поводу ключа</b></span></font></font></font></p>
<p style="margin-left: 1.91cm; margin-bottom: 0.28cm; border-top: none; border-bottom: 1.50pt solid #00000a; border-left: none; border-right: none; padding-top: 0cm; padding-bottom: 0.04cm; padding-left: 0cm; padding-right: 0cm; line-height: 108%">
<br/>
<br/>

</p>
<p style="margin-left: 1.91cm; margin-bottom: 0.28cm; line-height: 108%">
<br/>
<br/>

</p>
<p style="text-indent: 0.5cm; margin-bottom: 0.28cm; line-height: 108%">
<font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><b>Ограничение
по словам: </b></font></font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">основной
ключ &laquo;ключ&raquo; не использовать более раз, чем дано в
ключевых словах. То есть слово &laquo;ключ&raquo; можно использовать
только во вхождении, и нигде (никак) больше. Это относится ко всем
ключам и словоформам ключей.</font></font></p>
<p style="text-indent: 0.5cm; margin-bottom: 0.28cm; line-height: 108%">
<br/>
<br/>
</p>

<ol>
<?php foreach($wordarray as $keywords): ?>
<li>
	<p style="margin-bottom: 0cm; line-height: 100%"><font face="Times New Roman, serif" size="3" style="font-size: 12pt"><b><?php echo $keywords['title'] ?></b></font></p>
<br>
	<p class="western" style="margin-bottom: 0cm; line-height: 100%">
		<font face="Times New Roman, serif" size="3" style="font-size: 12pt">
		<b>Объем текста: <?php echo $keywords['size'] ?> знаков без пробелов</b>
		</font>
	</p>
<br>
	<p class="western" style="margin-bottom: 0.28cm; line-height: 108%">
		<font face="Times New Roman, serif" size="3" style="font-size: 12pt">
			<b>Ключевые слова: </b>
		</font>
	</p>
<br>
	<ol>
	<?php foreach($keywords['word'] as $word): ?>
		<li>
			<p class="western" style="margin-bottom: 0cm; line-height: 100%"><font face="Times New Roman, serif" size="3" style="font-size: 12pt">
		<?php echo $word; ?>
			</font></p></li>
		</li>
	<?php endforeach; ?>
	</ol>
</li>
<br>
<br>
<?php endforeach; ?>



</body>
</html>
